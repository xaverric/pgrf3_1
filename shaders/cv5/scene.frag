#version 150
// in hodnoty prijdou z start.fraf - vertex shaderu
in vec3 vertColor; // input from the previous pipeline stage
in vec2 textCoord;
in vec4 depthTextCoord;
out vec4 outColor; // output from the fragment shader
in vec3 normal; //input normal - normálový vektor
uniform sampler2D textureID;
uniform sampler2D textureDepth;

// Zdroj světla:
vec3 posLight;

void main() {
    posLight = vec3(0, 0, 10); // zdroj světla bude někde nad gridem - nad stredem sceny, jen X je nahore

	outColor = vec4(vertColor, 1.0);// - OK, ale není cerna

    // namapování textury:
//	outColor = texture(textureDepth, textCoord);

	// tímto bude koule i osvětlená difuzní složkou:
	// tím textureDepth se bude mapovat to hloubkove teleso vlevo na to teleso ve scene
	outColor.xyz = texture(textureDepth, textCoord).xyz * vertColor.xyz;


    // Aby se poznalo, jestli se je těloso ve stínu nebo ne (výpočet Z):
    float z1 = texture(textureDepth, depthTextCoord.xy / depthTextCoord.w).r;

    float z2 = depthTextCoord.z / depthTextCoord.w;

	bool shadow = z1 < (z2 - 0.0001); // to odecteni hodnoty je kvůli "doladovani", ze svetlo dopada sikmo na povrch telesa, tak je treba pricist konstantu ("takové vyhlazení stínů")

	if (shadow) {
	// je ve stinu:
	    outColor = vec4(1, 0, 1, 1.0);
	}
	else {
	    // zelena - není ve stinu:
	    outColor = vec4(0, 1, 0, 1.0);
	}
}