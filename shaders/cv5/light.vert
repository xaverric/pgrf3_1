#version 150
in vec2 inPosition; // input from the vertex buffer
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 matLightView;
uniform mat4 mathLightProj;
uniform int mode;
const float PI = 3.1459;


// je treba, aby bylo okno jako ctverec - kvůli projekci - bylo by treba upravit metodu reshape, aby to fungovalo normalne!
vec3 getSphere(vec2 xy) {
float azimut = xy.x * PI;
float zenit = xy.y * PI/2;
//float r = 1;

float x = 2*cos(azimut)*cos(zenit);
float y = 0.5*sin(azimut)*cos(zenit);
float z = sin(zenit);
return vec3(x, y, z);
}


void main() {
    // nasledujici operace jsou jedno a to samé, pouze jinak napsané - přepočet do okna na rozsah od -1 do 1
    vec2 pos = inPosition*2 - 1;;

    vec4 pos4;

    if (mode == 1) {
        // Zde se bere souradnice sphery
        pos4 = vec4(getSphere(pos), 1.0);
     }
    else {
        // zde se necha jen ten grid:
        pos4 = vec4(pos, 1, 1.0);
    }

    gl_Position = mathLightProj * matLightView * pos4;// toto je spravně
}