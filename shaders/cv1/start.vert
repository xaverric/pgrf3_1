#version 150
in vec2 inPosition; // input from the vertex buffer
//out vec3 vertColor; // output from this shader to the next pipeline stage
//uniform float time; // variable constant for all vertices in a single draw
uniform mat4 matMVP;

float computeZ(in vec2 pos){
float distance = sqrt(pos.x*pos.x+pos.y*pos.y);
//return cos(PI*scale*distance);
//return cos(PI*1/2*distance);
return cos(3.14*1/2*distance);
}



void main() {
    vec2 pos;

    // nasledujici operace jsou jedno a to samé, pouze jinak napsané - přepočet do okna na rozsah od -1 do 1

     //pos.x = inPosition.x*2-1;
     //pos.y = inPosition.y*2-1;
    // nebo:
     //pos = inPosition*vec2(2)-vec2(1, 1);
    // nebo:
     pos = inPosition*2 - 1;

vec2 position = 2.0 * (inPosition.xy - vec2(0.5));
//     float resultZ = computeZ(pos.xy);
     float resultZ = computeZ(position.xy);
//     float temp = 1/2*cos(sqrt(20*pos.x*pos.x+20*pos.y*pos.y));


// Za úkol: dodělat přepočet souřadnice a funkcí hodnot - aby grid byl ohnuty dle nějaké funkce!
// dále v p02geometry v p01cube zjistit jak funguje kamera, příště s tím budeme pracovat

	gl_Position = vec4(pos, 0.0, 1.0);
//	gl_Position = matMVP * vec4(pos.xy, resultZ, 1.0);
//	gl_Position = matMVP * vec4(pos.xy, temp, 1.0);
}


